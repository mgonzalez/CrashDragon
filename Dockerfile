# Build stage
FROM golang:alpine3.11 AS build
RUN apk add build-base git sassc autoconf automake pkgconf jsoncpp-dev jsoncpp-static
ENV CRASH_DRAGON_DIR /go/src/code.videolan.org/videolan/CrashDragon
WORKDIR $CRASH_DRAGON_DIR
COPY ./vendor $CRASH_DRAGON_DIR/vendor
RUN go get github.com/kardianos/govendor && /go/bin/govendor sync -v
COPY . $CRASH_DRAGON_DIR
RUN make prefix=/tmp/crashdragon install

# Final stage
FROM alpine:3.11
RUN apk add postgresql-client
COPY --from=build /tmp/crashdragon /usr/local
VOLUME /usr/local/share/crashdragon/files
WORKDIR /usr/local/bin
CMD until pg_isready -q -h postgres -U crashdragon; do sleep 1; done && crashdragon
